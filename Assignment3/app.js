var express = require('express');
var app = express();
var http = require('http').Server(app);
var port = process.env.POST || 8080;
var session = require('express-session');
var request = require('request');
var bodyParser = require('body-parser');
var XMLHttpRequest = require('xhr2')
var xhr = new XMLHttpRequest();

var firebase = require("firebase")

var config = {
    apiKey: "AIzaSyAuUFn-Fam0BOxOD0t0UXXxXUOL2X8jpk0",
    authDomain: "t305cde.firebaseapp.com",
    databaseURL: "https://t305cde.firebaseio.com",
    projectId: "t305cde",
    storageBucket: "t305cde.appspot.com",
    messagingSenderId: "685778375303"
};
firebase.initializeApp(config);

var database = firebase.database();

app.use(express.static(__dirname + '/css'));
app.use(express.static(__dirname + '/fonts'));
app.use(express.static(__dirname + '/vendor'));
app.use(express.static(__dirname + '/js'));
app.use(express.static(__dirname + '/images'));
app.use(express.static(__dirname + '/html'));
app.use(express.urlencoded({
    extended: true
}));

app.use(session({
    secret: 'abc',
    resave: true,
    saveUninitialized: true
}));

app.set('views', __dirname + '/');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');


app.get('/', function(req, res) {
    res.set('Content-Type', 'text/html');
    res.render("index.html", { message: "" });
    res.end();
});

app.get('/register', function(req, res) {
    res.set('Content-Type', 'text/html');
    res.render("register.html");
    res.end();
});

const Login = require("./login.js")
app.post('/login', function(req, res) {
    var user = req.body.username;
    var pass = req.body.pass;
    // req.session = null;
    Login.signin(database, user, pass, function(vaild) {
        if (vaild) {
            //   req.session.userid = "";
            console.log(user);
            req.session.userid = user;
            res.render("dashboard.html", { account: req.session.userid, message: "Manage the weather of cities" });
            res.end();
        }
        else {
            res.set('Content-Type', 'text/html');
            res.render("index.html", { message: "The username or password is incorrect" });
            res.end();
        }
    });
});


app.get('/favorite/:city',function(req, res) {

    var city = req.params.city;
    var ref = database.ref('/favorite/'+req.session.userid+city)
    var value = {
        user : req.session.userid,
        city : city
    }
    //inserted
    ref.set(value);

    res.render("dashboard.html", { message: "" , account: req.session.userid,});
    res.end();
});


app.get('/favorities',function(req, res) {
    
    var jsonArr = new Array();
    database.ref().child("favorite").once('value',function(snapshot){
        snapshot.forEach(function(account){
            if(req.session.userid == account.val().user){
                jsonArr.push(account.val());
           
            }
        });
        res.json({ favoriteData : jsonArr });
        res.end();
    });
});

app.get('/deleteFav/:city', function(req, res) {
    var city = JSON.stringify(req.params);
    var getCity = JSON.parse(city);
    database.ref("/favorite/").child(req.session.userid+getCity.city).remove();
    res.render('dashboard.html', { account: req.session.userid, message: "" });
    res.end();
});

app.get('/logout', function(req, res) {
    req.session.destroy();
    res.set('Content-Type', 'text/html');
    res.render("index.html", { message: "" });
    res.end();
});


app.post('/process', function(req, res) {

    var getOperator = req.body.operator;
    var city = req.body.city;
    if (city.indexOf(" ") != 0) {
        var operator = "";
        switch (getOperator) {
            case "GET":
                operator = "get";
                break;
            case "POST":
                operator = "post";
                break;
            case "PUT":
                operator = "put";
                break;
            case "DELETE":
                operator = "del";
                break;
            default:
                operator = "";
        }
        var url = 'http://assignment177113071api.herokuapp.com/' + operator + '/' + city;
        xhr.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var jsonArr = JSON.parse(this.responseText);
                res.render('dashboard.html', { account: req.session.userid, message: "successful" });
                res.end();
            }
        };
        xhr.open(getOperator, url, true);
        xhr.send();
    }
    else {
        res.render('dashboard.html', { account: req.session.userid, message: "Unsuccessful" });
        res.end();
    }

});



app.get('/get/:city', function(req, res) {
    var city = JSON.stringify(req.params);
    var getCity = JSON.parse(city);
    request('http://assignment177113071api.herokuapp.com/get/' + getCity.city, (err, response, body) => {
        //console.log(body);
        res.send(body);
        // res.render('dashboard.html', {message:body} );
        res.end();
    });
});



app.get('/getfuture/:city', function(req, res) {
    var city = JSON.stringify(req.params);
    var getCity = JSON.parse(city);
    request('http://assignment177113071api.herokuapp.com/getfuture/' + getCity.city, (err, response, body) => {
        res.send(body);
        res.end();
    });
});


app.get('/getAllcities/', function(req, res) {
    request('http://assignment177113071api.herokuapp.com/getAllcities', (err, response, body) => {
        res.send(body);
        res.end();
    });
});



app.get('/dashboard', function(req, res) {
    res.set('Content-Type', 'text/html');
    res.render('dashboard.html', { account: req.session.userid, message: "Manage the weather of cities" });
    res.end();
});

app.get('/forecast', function(req, res) {
    res.set('Content-Type', 'text/html');
    res.render('forecast.html', { account: req.session.userid, message: "Search the city on the above line", selectedCity: "" });
    res.end();
});

app.post('/processFuture', function(req, res) {
    var city = req.body.city;
    res.set('Content-Type', 'text/html');
    res.render('forecast.html', { account: req.session.userid, message: "Finding", selectedCity: city });
    res.end();
});

app.get('/processFutureWith/:city', function(req, res) {
    var city = JSON.stringify(req.params);
    var getCity = JSON.parse(city);
    res.set('Content-Type', 'text/html');
    console.log(getCity.city);
    res.render('forecast.html', {account: req.session.userid, message: "Finding", selectedCity: getCity.city });
    res.end();
});


// prepare for restful api

app.get('/post/:city', function(req, res) {
    var city = JSON.stringify(req.params);
    var getCity = JSON.parse(city);
    var url = 'http://assignment177113071api.herokuapp.com/post/' + getCity.city;

    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var jsonArr = JSON.parse(this.responseText);
            console.log(jsonArr);
            res.render('dashboard.html', { account: req.session.userid, message: "successful" });
            res.end();
        }
    };
    xhr.open('POST', url, true);
    xhr.send();
});

app.get('/put/:city', function(req, res) {
    var city = JSON.stringify(req.params);
    var getCity = JSON.parse(city);
    var url = 'http://assignment177113071api.herokuapp.com/put/' + getCity.city;

    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var jsonArr = JSON.parse(this.responseText);
            res.render('dashboard.html', { account: req.session.userid, message: "successful" });
            res.end();
        }
    };
    xhr.open('PUT', url, true);
    xhr.send();
});

app.get('/del/:city', function(req, res) {
    var city = JSON.stringify(req.params);
    var getCity = JSON.parse(city);
    var url = 'http://assignment177113071api.herokuapp.com/del/' + getCity.city;

    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var jsonArr = JSON.parse(this.responseText);
            res.render('dashboard.html', { account: req.session.userid, message: "successful" });
            res.end();
        }
    };
    xhr.open('DELETE', url, true);
    xhr.send();
});



http.listen(port, function() {
    console.log('Server Port : ' + port);
});
