
  // Initialize Firebase
var config = {
    apiKey: "AIzaSyAuUFn-Fam0BOxOD0t0UXXxXUOL2X8jpk0",
    authDomain: "t305cde.firebaseapp.com",
    databaseURL: "https://t305cde.firebaseio.com",
    projectId: "t305cde",
    storageBucket: "t305cde.appspot.com",
    messagingSenderId: "685778375303"
  };
  firebase.initializeApp(config);

var allAccountName = new Array();

document.getElementById('register').onclick = register;


//register a new account -- limit the form
function checkEmpty(x) {
    if ( x == null || x == "" ) {
      return false;
   }
   return true;
}


//register account & insert data
function register(){
  var database = firebase.database();
	var alldata = document.querySelectorAll('#registerForm input');
  var id = new Date().getTime();
	var account = {
	    uniqueid : id,
	    username : alldata[1].value,
    	name : alldata[2].value,
    	email : alldata[3].value,
    	password : alldata[4].value,
	    age : alldata[5].value
    	};
    //limit the null input
	if(checkEmpty(account.username) && checkEmpty(account.name) 
	    && checkEmpty(account.email) && checkEmpty(account.password) && checkEmpty(account.age) ){
	     var state = false;
	     //check account duplication
	     for(var i = 0; i<allAccountName.length; i++){
	         if(account.username == allAccountName[i]){
	           alert(account.username + " <--account & table -->  " + allAccountName[i]);
	           state = true;
	           break;
	           }
	     }
	     if(state == false){
	         //post to the firebase by Ajax
	            var request = new XMLHttpRequest();
                request.open('POST', 'https://t305cde.firebaseio.com/Accounts/.json', false);
                request.send(JSON.stringify(account));
	          
	        }else{
	          alert('cannot create the account');
	        }
	}else{
	    alert("please fill in the form");
	}
}

//create an Object Array for sort 
var objAccount = new Array();
//display to Table 
function showAccounts(){
    var database = firebase.database();
    var totelAge = 0; 
    var count = 0; 
    var list = '<tr>';
    list += '<td>Username</td>';
    list += '<td onclick="sortTableByName();">Name</td>';
    list += '<td onclick="sortTableByAge();">Age</td>';
    list += '</tr>';
    //var list = '';
    database.ref().child("Accounts").once('value',function(snapshot){
        snapshot.forEach(function(account){
            count = count + 1;
            var uniqueid =account.val().uniqueid;
            var username = account.val().username;
            var name = account.val().name;
            var email = account.val().email;
            var password = account.val().password;
            var age = account.val().age;
                objAccount.push({'username' : username, 'name' : name , 'age' : parseInt(age) });
                var content ='<tr>';
                content += '<td>' +  username + '</td>';
                content += '<td>' +  name + '</td>';
                content += '<td>' +  age + '</td>';
                content += '</tr>';
                allAccountName.push(username);
                totelAge += parseInt(age);
                list += content;
        });
        //calculate the avg of age & display in the table
        var calAge = totelAge / count; 
        document.getElementById('avgAge').innerHTML = "The average age of total members is " +calAge;
        document.getElementById('displayAccounts').setAttribute("border",1);
        document.getElementById('displayAccounts').innerHTML = list;
    });
}
//display the spec data to the table
document.getElementById('showSpecAccounts').onclick = showSelectedAccounts ;
function showSelectedAccounts() {
      var totelAge = 0; 
    var count = 0; 
   var database = firebase.database();
   var specAge= document.getElementById('specAge').value;
   var operator = document.getElementById('operator');
   var selectedOperator = operator.options[operator.selectedIndex].value;
   var list = '<tr>';
    objAccount =[];
    list += '<td>Username</td>';
    list += '<td onclick="sortTableByName();">Name</td>';
    list += '<td onclick="sortTableByAge();">Age</td>';
    list += '</tr>';
     database.ref().child("Accounts").once('value',function(snapshot){
        snapshot.forEach(function(account){
            // check the operator
            if(selectedOperator == ">="){
              if(parseInt(account.val().age) >= parseInt(specAge)){
                  var username = account.val().username;
                  var name = account.val().name;
                  var age = account.val().age;
                  var content ='<tr>';
                content += '<td>' +  username + '</td>';
                content += '<td>' +  name + '</td>';
                content += '<td>' +  age + '</td>';
                content += '</tr>';
                objAccount.push({'username' : username, 'name' : name , 'age' : parseInt(age) });
                 totelAge += parseInt(age);
                 count = count + 1;
                list += content;
              }
            }else{
                
                  if(parseInt(account.val().age) <= parseInt(specAge) ){
                  var username = account.val().username;
                  var name = account.val().name;
                  var age = account.val().age;
                  var content ='<tr>';
                list += content;
                content += '<td>' +  username + '</td>';
                content += '<td>' +  name + '</td>';
                content += '<td>' +  age + '</td>';
                content += '</tr>';
                objAccount.push({'username' : username, 'name' : name , 'age' : parseInt(age) });
                totelAge += parseInt(age);
                 count = count + 1;
                list += content;
               }
            }
        });
        var calAge = totelAge / count; 
        document.getElementById('avgAge').innerHTML = "The average age of total members is " +calAge;
        document.getElementById('displayAccounts').innerHTML = list;
     });
   
}



//sort table by name
function sortTableByName(){
    objAccount.sort(function (a ,b) {
        return((a.name < b.name) ? -1 : ((a.name == b.name) ? 0 : 1));
    });
    var list = '<tr>';
    list += '<td>Username</td>';
    list += '<td onclick="sortTableByName();">Name</td>';
    list += '<td onclick="sortTableByAge();">Age</td>';
    list += '</tr>';
    for(var i= 0; i<objAccount.length; i++){
        list += "<tr><td>"+ objAccount[i].username +"</td><td>"+ objAccount[i].name+"</td><td>" + objAccount[i].age+"</td><tr>";
        
    }
    document.getElementById('displayAccounts').innerHTML = list;
}

//sort by age
function sortTableByAge(){
    objAccount.sort(function (a ,b) {
        return((a.age < b.age) ? -1 : ((a.age == b.age) ? 0 : 1));
    });
    var list = '<tr>';
    list += '<td>Username</td>';
    list += '<td onclick="sortTableByName();">Name</td>';
    list += '<td onclick="sortTableByAge();">Age</td>';
    list += '</tr>';
    for(var i= 0; i<objAccount.length; i++){
        list += "<tr><td>"+ objAccount[i].username +"</td><td>"+ objAccount[i].name+"</td><td>" + objAccount[i].age+"</td><tr>";
    }
    document.getElementById('displayAccounts').innerHTML = list;
}


//another method retieve data from firebase 

// Retieve data from firebase 
// function showAccounts(){
//     var database = firebase.database();
//     var ref = database.ref("Accounts");
//     ref.on('value',gotData,errData);
// }
// function gotData(data)
// {
//   var Accounts = data.val();
//     var keys = Object.keys(Accounts);
//     console.log(keys);
//     for (var i = 0; i< keys.length; i++){
//         var k = keys[i];
//         var uniqueid = Accounts[k].uniqueid;
//         var name = Accounts[k].name;
//         alert(uniqueid + "   " + name);
//     }
// }
// function errData(err){ 
//     console('Error!');
//     console.log(err);
// }

	