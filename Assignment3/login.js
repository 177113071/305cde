exports.signin = function(database, username, password, callback){
    var objAccount = new Array();
    database.ref().child("Accounts").once('value',function(snapshot){
        snapshot.forEach(function(account){
           objAccount.push(account.val());
        });
        var vaild = false;
        for(var i = 0; i < objAccount.length; i++){
            if (objAccount[i].username == username && objAccount[i].password == password){
                vaild = true;
                break;
            }else{
                vaild = false;
            }
        }
        callback(vaild);
    });
}