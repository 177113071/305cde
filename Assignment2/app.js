var express  = require( 'express' );
var http  = require( 'http' );
var app = express();

app.set( 'port', process.env.PORT || 3000 );
app.get('/', function (req, res) {
    res.send('Hello World');
});

// listen the server in host : 8000
app.listen(10080, function () {
  console.log('%s listening at %s', app.name, app.url);
});

http.createServer( app ).listen( app.get( 'port' ), function (){
  console.log( 'Express server listening on port ' + app.get( 'port' ));
});



  // Initialize Firebase
var firebase = require("firebase")

var config = {
    apiKey: "AIzaSyAuUFn-Fam0BOxOD0t0UXXxXUOL2X8jpk0",
    authDomain: "t305cde.firebaseapp.com",
    databaseURL: "https://t305cde.firebaseio.com",
    projectId: "t305cde",
    storageBucket: "t305cde.appspot.com",
    messagingSenderId: "685778375303"
  };
  firebase.initializeApp(config);


//prepare the server key setting
var restify = require('restify');
var request = require('request')
const server = restify.createServer({
  name: 'myapp',
  version: '1.0.0'
});

const link = require("./CityListener.js")

// heroku testing

app.use(restify.plugins.acceptParser(server.acceptable));
app.use(restify.plugins.queryParser());
app.use(restify.plugins.bodyParser());
 
 
 
// check the open weather api data to display 
app.get('/desc/:city', function (req, res) {
  var city = JSON.stringify(req.params);
  var getCity = JSON.parse(city);
  request.get(link.link(getCity.city), (err,res2,body)=>{
    //display the parsing data 
    var obj = JSON.parse(body);
     res.write("<html><body><div='container'>");
     res.write("<h1>" +'City Name - : ' + obj['name'] + "</h1>");
     res.write("<h3>" + 'Template - : ' +obj.main['temp'] + '&#176C' +"</h3>");
     res.write("<h3>" + 'Weather Description - : ' +obj.weather[0].description + '<br/>' +"</h3>");
     res.write("<h3>" + 'Template of Max - : ' +obj.main.temp_max + '&#176C' +"</h3>");
     res.write("<h3>" + 'Template of Min - : ' +obj.main.temp_min + '&#176C' +"</h3>");
     res.write("<h3>" + 'SunSet Time - : ' + new Date(obj.sys['sunset']*1000)+"</h3>");
     res.write("</div></body</html>");
     res.end();
  });
});
//check the open weather api data in json format
app.get('/api/:city', function (req, res) {
  var city = JSON.stringify(req.params);
  var getCity = JSON.parse(city);
    //get the data with json format from the api link
    request.get('http://api.openweathermap.org/data/2.5/weather?q='+getCity.city+'&appid=4be6043d5299983905a8b91114ef6baa&units=metric', (err,res2,body)=>{
    res.end();
  });
});

app.get('/getfuture/:city', function (req, res) {
  var city = JSON.stringify(req.params);
  var getCity = JSON.parse(city);
  request.get('http://api.openweathermap.org/data/2.5/forecast?q='+getCity.city+'&appid=4be6043d5299983905a8b91114ef6baa&units=metric', (err,res2,body)=>{
      var obj = JSON.parse(body);
      res.send({list : obj.list});
      res.end();
  });

  
});

// ----------restify api------------------------------
  
  
var database = firebase.database();
// get 0 all cities

const displayCities = require("./displayCities.js")
app.get('/allcities', function (req, res) {
    //call the displayCities class
    displayCities.allcities(database, res);
});


// get - the city json
app.get('/get/:city', function (req, res) {
  var city = JSON.stringify(req.params);
  var getCity = JSON.parse(city);
  database.ref().child("openWeather/"+getCity.city).once('value',function(snapshot){
    //get the json format data by city 
      res.write(JSON.stringify(snapshot.val()));
  res.end();
  });
});

// get - All cities Json
app.get('/getAllCities', function (req, res) {
  var city = JSON.stringify(req.params);
  var getCity = JSON.parse(city);
  database.ref().child("openWeather").once('value',function(snapshot){
      res.write(JSON.stringify(snapshot.val()))
  res.end();
  });
});

 
// post the data to firebase
const postData = require("./postData.js")
app.post('/post/:city', function (req, res) {
  var city = JSON.stringify(req.params);
  var getCity = JSON.parse(city);
  postData.post(database,getCity.city);
  res.send({message : getCity.city + ' inserted to the database '});
  res.end();
});


// put - update the data 

app.put('/put/:city', function (req, res) { 
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = now.getFullYear() + "-" + (month) + "-" + (day);
  var cities = new Array();
  var city = JSON.stringify(req.params);
  var getCity = JSON.parse(city);
  database.ref().child("openWeather").once('value',function(snapshot){
        snapshot.forEach(function(weather){
            var city = weather.val().city;
            cities.push(city);
        });
        // check the data by boolean
        var noRecord = true;
        for(var i = 0; i < cities.length; i++){
          if(cities[i] == getCity.city){
              request.get(link.link(getCity.city), (err,res2,body)=>{
              var obj = JSON.parse(body);
              //update the firebase 
              database.ref("/openWeather/").child(getCity.city).update({
                  city : getCity.city,
                  template : obj.main['temp'],
                  description : obj.weather[0].description,
                  temp_max : obj.main.temp_max,
                  temp_min : obj.main.temp_min,
                  date : today
                  });
              });
              noRecord = false;
              res.send({message : 'The database update '+ getCity.city + ' record'});
              break;
          }else{
              noRecord = true;
          }
        }
        if(noRecord == true){
            res.send({message : 'The database don\'t have '+ getCity.city + ' record'});

        }
        res.end();
  });

});

// delete - del the data by city
app.del('/del/:city', function (req, res) { 
  var city = JSON.stringify(req.params);
  var getCity = JSON.parse(city);
  //del the item using firebase remove()
  database.ref("/openWeather/").child(getCity.city).remove();
  res.send({message : getCity.city + ' is deleted.'})
  res.end();
});



// home page
// app.get('/', function (req, res) {
//   res.send('Hello World!');
// }); 
 
 

