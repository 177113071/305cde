exports.allcities = function(database,res){
    res.write("<html><body><div='container'>");
    var weather_array = new Array();
    // display the firebase data
    database.ref().child("openWeather").once('value',function(snapshot){
        snapshot.forEach(function(weather){
          weather_array.push(weather.val());
        });
        //loop the data to output
        for(var i = 0; i< weather_array.length; i++ ){
          res.write("<h1>" +'City Name - : ' +weather_array[i].city+ "</h1>");
          res.write("<h3>" + 'Template - : ' +weather_array[i].template+'&#176C' +"</h3>");
          res.write("<h3>" + 'Weather Description - : ' + weather_array[i].description + '<br/>' +"</h3>");
          res.write("<h3>" + 'Template of Max - : ' +weather_array[i].temp_max + '&#176C' +"</h3>");
          res.write("<h3>" + 'Template of Min - : ' +weather_array[i].temp_min + '&#176C' +"</h3>");
          res.write("<br/>");
        }
        res.write("</div></body</html>");
        res.end();
  });
}