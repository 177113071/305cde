var restify = require('restify');
var server = restify.createServer();
var nano = require('nano')('http://localhost:5984');


var articles = nano.use('articles');

server.use(restify.fullResponse()).use(restify.bodyParser())
    
server.post("/articles",createArticle)
server.get("/articles/:id",viewArticle)

var port = process.env.POST || 3000
server.listen(port,function (err){
   if (err) {
       console.log(err);
   }else{
       console.log('App is ready at : ' + port);
   }
});

function createArticle(req, res , next){
    articles.insert(req.body, function(err,body){
        if(!err){
            res.json({result: "success",data :body});
        }else{
            res.json(err);
        }
        res.send();
    });
}

function viewArticle(req, res ,next) {
    articles.get(req.params.id ,function(err, body){
       if(!err){
           res.json({result:"success", data : body})
       } else{
            res.json(err);
       }
       res.send();
    });
}